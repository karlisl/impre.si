<?php
/**
 * print_r + var_dump
 *
 * @param array $data
 * @param boolean $var_dump
 */
function pr($data = null, $var_dump = false) {
	if ($var_dump) {
		var_dump($data);
	} else {
		echo '<pre>';
			print_r($data);
		echo '</pre>';
	}
}

/**
 * (e)mpty (s)afe (e)cho - echoes $string or $empty, if $string is empty
 *
 * @param string $string to echo
 * @param string $empty string to echo, if empty (default is empty string)
 * @return void echoes, returning nothing
 */
function ese(&$string, $empty="") {
	if(!empty($string)) {
		echo $string;
	}
	else {
		echo $empty;
	}
}

/**
 * (e)mpty (s)afe (r)eturn - returns $val or $empty, if $val is empty
 *
 * @param mixed $val to return
 * @param mixed $empty value to return, if empty (default is null)
 * @return mixed $val or $empty
 */

function esr(&$val, $empty = null) {
	if(!empty($val)) {
		return $val;
	}
	else {
		return $empty;
	}
}

function query_log() {
	if (isset($_SESSION['query_log'])) {
		pr($_SESSION['query_log']);
	} else {
		pr(array());
	}
}

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
?>