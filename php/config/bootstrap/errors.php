<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

use lithium\core\ErrorHandler;
use lithium\action\Response;
use lithium\net\http\Media;
use lithium\core\Environment;
use lithium\analysis\Logger;

Logger::config(array(
	'error' => array(
		'adapter' => 'File',
		'priority' => array('emergency', 'alert', 'critical', 'error')
	),
	'debug' => array(
		'adapter' => 'File',
		'priority' => array('notice', 'info', 'debug')
	)
));

/**
 * Kļūdu paziņojumu apstrādātājs
 */
ErrorHandler::apply('lithium\action\Dispatcher::run', array(), function($info, $params) {
	$response = new Response(array('request' => $params['request']));
	$environment = Environment::get();

	$dev_ips = array(
		'127.0.0.1',
		'88.135.141.92',
		'195.244.135.169'
	);

	/**
	 * Izstrādātājiem izmantojam 'development' kļūdu skatu,
	 * citos gadījumos izmantojam Environment::get() atgrieztās izstrādes vides skatu.
	 * Noklusētais layout ir 'error'.
	 */
	$template = in_array(getenv('REMOTE_ADDR'), $dev_ips) ? 'development' : $environment;
	$layout = 'error';

	/**
	 * Ja esam uz dzīvajiem serveriem un neesam izstrādātāji, tad izvēlamies attiecīgo kļūdu
	 * skatu un izmantojam 'default' layout.
	 */
	if (in_array($template, array('production', 'test', 'development')) && !in_array(getenv('REMOTE_ADDR'), $dev_ips)) {
		$code = $info['exception']->getCode();

		/**
		 * Iespējamie kļūdu skati.
		 * Ja neeksistē attiecīgā kļūdas koda skats, tad rādam skatu '404'.
		 */
		$code_templates = array(
			'404'
		);

		if (in_array($code, $code_templates)) {
			$template = $code;
		} else {
			$template = '404';
		}

		$layout = 'default';
	}

	/**
	 * Ierakstam 'error' logā paziņojumus, kuru veids nav
	 * - RoutingException
	 * - DispatchException
	 */
	switch (true) {
		case preg_match('/^RoutingException/', $info['type']):
		case preg_match('/^DispatchException/', $info['type']):
			break;

		default:
			Logger::write('error', "{$info['file']} : {$info['line']} : {$info['message']}");
			break;
	}

	Media::render($response, compact('info', 'params'), array(
		'controller' => '_errors',
		'template' => (string) $template,
		'layout' => $layout,
		'request' => $params['request'],
		'data' => array(
			'locale' => !empty($params['request']->locale) ? $params['request']->locale : Environment::get('locale'),
			'environment' => $environment
		)
	));
	return $response;
});
?>