<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * The routes file is where you define your URL structure, which is an important part of the
 * [information architecture](http://en.wikipedia.org/wiki/Information_architecture) of your
 * application. Here, you can use _routes_ to match up URL pattern strings to a set of parameters,
 * usually including a controller and action to dispatch matching requests to. For more information,
 * see the `Router` and `Route` classes.
 *
 * @see lithium\net\http\Router
 * @see lithium\net\http\Route
 */
use lithium\net\http\Router;
use lithium\core\Environment;
use lithium\storage\Session;

use app\models\Pages;

Router::connect('/', 'Pages::index');

Router::connect(
	'/admin/{:controller}/{:action}/{:args}',
	array(
		'admin' => true
	),
	function($request) {
		$request->params['action'] = str_replace('-', '_', $request->params['action']);

		if (!Session::check('user', array('name' => 'default'))) {
			$request->params['controller'] = 'users';
			$request->params['action'] = 'login';
			unset($request->params['admin']);

			return $request;
		}

		$request->persist = array(
			'admin',
			'controller'
		);

		return $request;
	}
);

if (Session::check('user', array('name' => 'default'))) {
	Router::connect(
		'/admin/{:args}',
		array(
			'admin' => true,
			'controller' => 'pages',
			'action' => 'index'
		),
		array(
			'persist' => array(
				'admin',
				'controller'
			)
		)
	);
} else {
	Router::connect(
		'/admin/{:args}',
		array(
			'controller' => 'users',
			'action' => 'login'
		)
	);
}

Router::connect(
	'/tpl/{:args}',
	array('controller' => 'templates', 'action' => 'serve')
);

Router::connect(
	'/{:action}/{:args}',
	array('controller' => 'pages'),
	function($request) {
		$request->params['action'] = str_replace('-', '_', $request->params['action']);
		return $request;
	}
);
?>