<?php

namespace app\extensions\adapter\cache;

use Redis as RedisCore;

class Redis extends \lithium\storage\cache\adapter\Redis {

	public function command($command, $args) {
		$connection =& $this->connection;

		if ($result = call_user_func_array(array($connection, $command), $args)) {
			return $result;
		}

		return false;
	}

	public function rpush($key, $value = null) {
		$connection =& $this->connection;

		if ($result = $connection->rpush($key, $value)) {
			return $result;
		}

		return false;
	}

	public function hset($key, $hashKey = null, $value = null) {
		$connection =& $this->connection;

		if ($result = $connection->hset($key, $hashKey, $value)) {
			return $result;
		}

		return false;
	}

	public function blpop($keys, $timeout = 0) {
		$connection =& $this->connection;

		if ($result = $connection->blpop($keys, $timeout)) {
			return $result;
		}

		return false;
	}

	public function publish($channel, $message) {
		$connection =& $this->connection;

		if ($result = $connection->publish($channel, $message)) {
			return $result;
		}

		return false;
	}
};
