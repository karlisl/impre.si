<?php

namespace app\extensions\image;

/** EXAMPLE:

	$wm = new ImageWatermarking();

	$wm->position = 3; // bottom-right corner
	#$wm->position = array(50,100); // custom

	$wm->quality = 90;

	$wm->repeat = false; //default

	$wm->scale = false; //default

	$wm->chmod = 0777;

	$r = $wm->watermark("original.jpg", "watermark.png", "output.jpg");

	if (!$r)
	{
		die("Error #".$wm->error);
	}

*/


/**
 * Image watermarking class
 *
 * Avalaible functions:
 * watermark, getExtension
 *
 * Avalaible variables:
 * quality, position, scale, repeat, error, chmod
 */
class Watermarking {
	/**
	 * If an errors occurs, it's number is stored here:
	 * 1: input file not found
	 * 2: unsupported input filetype
	 * 3: unsupported output filetype
	 * 4: unsupported watermark filetype
	 * 5: could not create new image
	 * 6: could not copy watermark image
	 * 7: could not save new image
	 */
	var $error = 0;

	/**
	 * Quality/compression
	 * For JPG its quality (1-100), for PNG - compression (0-9). GIF
	 * doesn't have any of those
	 */
	var $quality = 100;

	/**
	 * What permissions should be applied to destination image
	 */
	var $chmod = 0644;

	/**
	 * Watermark position (default is centered)
	 * 1 - top-left corner
	 * 2 - top-right corner
	 * 3 - bottom-right corner
	 * 4 - bottom-left corner
	 * 5 - center
	 * array(x, y) - custom
	 */
	var $position = 5;

	/**
	 * Should watermark be repeated? Default is false. This is ignored if $this->scale is set to true
	 * or $this->position is custom (array)
	 */
	var $repeat = false;


	/**
	 * If true, watermark will be scaled fullsize. If true $this->position and $this->repeat
	 * is not taken into account.  This is ignored if $this->position is custom (array)
	 */
	var $scale = false;

	/**
	 * Watermark image
	 *
	 * @param string $input_path Source path
	 * @param string $watermark_path Watermark image's path
	 * @param string $output_path Destination path (optional)
	 *
	 * @return boolean
	 */
	function watermark($input_path, $watermark_path, $output_path = null)
	{
		// reset error
		$this->error = 0;

		// check if input file exists
		if (!file_exists($input_path))
		{
			$this->error = 1;
			return false;
		}

		// output path is not defined, save over original image
		if ($output_path == null)
		{
			$output_path = $input_path;
		}

		// get input/output file type
		$input_extension = $this->getExtension($input_path);
		$output_extension = $this->getExtension($output_path);
		$watermark_extension = $this->getExtension($watermark_path);

		if (!in_array($output_extension, array("jpg", "png", "gif")))
		{
			$this->error = 3;
			return false;
		}

		if (!in_array($watermark_extension, array("jpg", "png", "gif")))
		{
			$this->error = 4;
			return false;
		}

		// get size of the original image
		list($img_im_w, $img_im_h) = getimagesize($input_path);

		// get size of the watermark image
		list($img_wm_w, $img_wm_h) = getimagesize($watermark_path);


		switch ($input_extension)
		{
			case "jpg":
			$src_im = imagecreatefromjpeg($input_path);
			break;

			case "png":
			$src_im = imagecreatefrompng($input_path);
			break;

			case "gif":
			$src_im = imagecreatefromgif($input_path);
			break;

			default:
			$this->error = 2;
			return false;
			break;
		}

		if (!isset($src_im) || !$src_im)
		{
			imagedestroy($src_im);
			$this->error = 5;
			return false;
		}

		switch ($watermark_extension)
		{
			case "jpg":
			$src_wm = imagecreatefromjpeg($watermark_path);
			break;

			case "png":
			$src_wm = imagecreatefrompng($watermark_path);
			break;

			case "gif":
			$src_wm = imagecreatefromgif($watermark_path);
			break;

			default:
			$this->error = 2;
			return false;
			break;
		}

		if (!isset($src_wm) || !$src_wm)
		{
			imagedestroy($src_wm);
			$this->error = 5;
			return false;
		}

		if ($this->scale)
		{
			$r = imagecopyresampled($src_im, $src_wm, $pos_x, $pos_y, 0, 0, $img_im_w, $img_im_h, $img_wm_w, $img_wm_h);
		}
		else
		if ($this->repeat)
		{
			if (is_array($this->position))
			{
				$this->position = 5;
			}

			switch ($this->position)
			{
				case 1: // top-left
				for ($y=0; $y<$img_im_h; $y+=$img_wm_h)
				{
					for ($x=0; $x<$img_im_w; $x+=$img_wm_w)
					{
						$r = imagecopy($src_im, $src_wm, $x, $y, 0, 0, $img_wm_w, $img_wm_h);
					}
				}
				break;


				case 2: // top-right
				for ($y=0; $y<$img_im_h; $y+=$img_wm_h)
				{
					for ($x=$img_im_w; $x>-$img_wm_w; $x-=$img_wm_w)
					{
						$r = imagecopy($src_im, $src_wm, $x, $y, 0, 0, $img_wm_w, $img_wm_h);
					}
				}
				break;


				case 3: // bottom-right
				for ($y=$img_im_h; $y>-$img_wm_h; $y-=$img_wm_h)
				{
					for ($x=$img_im_w; $x>-$img_wm_w; $x-=$img_wm_w)
					{
						$r = imagecopy($src_im, $src_wm, $x, $y, 0, 0, $img_wm_w, $img_wm_h);
					}
				}
				break;


				case 4: // bottom-left
				for ($y=$img_im_h; $y>-$img_wm_h; $y-=$img_wm_h)
				{
					for ($x=0; $x<$img_im_w; $x+=$img_wm_w)
					{
						$r = imagecopy($src_im, $src_wm, $x, $y, 0, 0, $img_wm_w, $img_wm_h);
					}
				}
				break;


				case 5: // center

				$pos_x = -(($img_im_w%$img_wm_w)/2);
				$pos_y = -(($img_im_h%$img_wm_h)/2);

				for ($y=$pos_y; $y<$img_im_h; $y+=$img_wm_h)
				{
					for ($x=$pos_x; $x<$img_im_w; $x+=$img_wm_w)
					{
						$r = imagecopy($src_im, $src_wm, $x, $y, 0, 0, $img_wm_w, $img_wm_h);
					}
				}
				break;
			}

		}
		else
		{
			// custom location
			if (is_array($this->position))
			{
				list($pos_x, $pos_y) = $this->position;
			}
			// predefined location
			else
			{
				switch ($this->position)
				{
					case 1: // top-left
					$pos_x = 0;
					$pos_y = 0;
					break;

					case 2: // top-right
					$pos_x = $img_im_w - $img_wm_w;
					$pos_y = 0;
					break;

					case 3: // bottom-right
					$pos_x = $img_im_w - $img_wm_w - 10;
					$pos_y = $img_im_h - $img_wm_h - 5;
					break;

					case 4: // bottom-left
					$pos_x = 0;
					$pos_y = $img_im_h - $img_wm_h;
					break;

					case 5: // center
					$pos_x = round(($img_im_w - $img_wm_w) / 2);
					$pos_y = round(($img_im_h - $img_wm_h) / 2);
					break;
				}
			}

			$r = imagecopy($src_im, $src_wm, $pos_x, $pos_y, 0, 0, $img_wm_w, $img_wm_h);
		}

		if (!$r)
		{
			$this->error = 6;
			return false;
		}

		switch ($output_extension)
		{
			case "jpg":
			if ($this->quality < 0 || $this->quality > 100)
			{
				$this->quality = 100;
			}
			$r = imagejpeg($src_im, $output_path, $this->quality);
			break;

			case "png":
			if ($this->quality < 0 || $this->quality > 9)
			{
				$this->quality = 9;
			}
			$r = imagepng($src_im, $output_path, $this->quality);
			break;

			case "gif":
			$r = imagegif($src_im, $output_path);
			break;
		}

		if (!$r)
		{
			$this->error = 7;
			return false;
		}


		chmod($output_path, $this->chmod);

		imagedestroy($src_im);

		return true;
	}


	/**
	 * Get file extension
	 *
	 * @param string $filename Filename
	 * @return string
	 */
	function getExtension($filename)
	{
		$pos = strrpos($filename, ".");

		if ($pos === false)
		{
			return "";
		}

		return strtolower(substr($filename, $pos + 1));
	}
}

?>