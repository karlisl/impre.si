<?php

namespace app\extensions\image;

/**
 * Image resizer class 1.0.4
 *
 * Avalaible functions:
 * resize, getExtension, getFileExtensionByMimetype
 *
 * Avalaible variables:
 * quality, compression, units, enlarge, error, chmod, force_input_type, keep_ratio
 *
 * CHANGELOG:
 *
 * v1.0.0 (2008-05-01)
 * 	* 1st release
 *
 * v1.0.1 (2008-05-17)
 * 	* added $force_input_type, getFileExtensionByMimetype()
 * 	* changed get_extension function name to getExtension
 *
 * v1.0.2 (2008-06-16)
 *	* image size is now calculated after source image is read (if input file is not an image, /0 error is not thrown anymore)
 *
 * v1.0.3 2008-08-28)
 * 	* if output path (directories) don't exist, resizer tries to create them
 * 	* added $keep_ratio, $compression
 *
 * v1.0.4 2009-06-19)
 * 	* added $force_input_extension
 *
 *
 */
class Resizer {
	/**
	 * Output image's quality.
	 * Currently only JPG (value 1-100) supports quality param
	 */
	var $quality = 100;

	/**
	 * Output image's compression.
	 * Currently only PNG (value 1-9) supports compression param
	 */
	var $compression = 9;

	/**
	 * Scale units. Percents ('%') and pixels ('px') are avalaible
	 * Default is 'px'
	 */
	var $units = 'px';

	/**
	 * if set to false and width or height of the destination image is bigger
	 * than source image's width or height, then leave source image's dimensions
	 * untached
	 */
	var $enlarge = true;

	/**
	 * If an errors occurs, it's number is stored here:
	 * 1: input file not found
	 * 2: unsupported input filetype
	 * 3: unsupported output filetype
	 * 4: could not create new image
	 * 5: could not create canvas for new image
	 * 6: could not resize image
	 * 7: could not save new image
	 * 8: could not create output path
	 */
	var $error = 0;

	/**
	 * What permissions should be applied to destination image
	 */
	var $chmod = 0644;

	/**
	 * If not empty, force this script to think this is real file
	 * type. This is good way to pass freshly uploaded file which
	 * by default is without any extension so this script cannot
	 * determine its type
	 */
	var $force_input_type = '';

	/**
	 * Same as force_input_type, but with extension
	 */
	var $force_input_extension = '';

	/**
	 * If true and both output width and height is specified and
	 * crop is set to false, image is resized with respect to it's
	 * original ratio. If false (default), image is simple scaled.
	 */
	var $keep_ratio = false;


	/**
	 * Resize image
	 *
	 * @param string $input_path Source path
	 * @param string $output_path Destination path
	 * @param int $output_width Output image's width
	 * @param int $output_height Output image's height
	 * @param boolean $crop Should image be cropped to maintain aspect ratio when needed?
	 * @return boolean
	 */
	function resize($input_path, $output_path, $output_width = null, $output_height = null, $crop = true)
	{
		// reset error
		$this->error = 0;

		// if output path (directories) doesn't exist, try to make whole path

		$arr_output_path = explode(DS, $output_path);

		unset($arr_output_path[count($arr_output_path)-1]);

		$dir_path = implode($arr_output_path, DS);

		if (!file_exists($dir_path))
		{
			if (!mkdir($dir_path, 0777, true))
			{
				$this->error = 8;
				return false;
			}
		}

		// check if input file exists
		if (!file_exists($input_path))
		{
			$this->error = 1;
			return false;
		}

		// get input/output file type

		if ($this->force_input_type != '')
		{
			$input_extension = $this->getFileExtensionByMimetype($this->force_input_type);
		}
		else
		if ($this->force_input_extension != '')
		{
			$input_extension = $this->force_input_extension;
		}
		else
		{
			$input_extension = $this->getExtension($input_path);
		}

		$output_extension = $this->getExtension($output_path);

		if (!in_array($output_extension, array('jpg', 'png', 'gif')))
		{
			$this->error = 3;
			return false;
		}

		switch ($input_extension)
		{
			case 'jpg':
			$src_im = imagecreatefromjpeg($input_path);
			break;

			case 'png':
			$src_im = imagecreatefrompng($input_path);
			break;

			case 'gif':
			$src_im = imagecreatefromgif($input_path);
			break;

			default:
			$this->error = 2;
			return false;
			break;
		}

		if (!isset($src_im) || !$src_im)
		{
			imagedestroy($src_im);
			$this->error = 4;
			return false;
		}

		// calculate new w, h, x and y

		// get size of the original image
		list($input_width, $input_height) = getimagesize($input_path);

		//calculate destination image w/h

		// turn % into px
		if ($this->units == '%')
		{
			if ($output_height != null)
			{
				$output_height = round($input_height * $output_height / 100);
			}

			if ($output_width != null)
			{
				$output_width = round($input_width * $output_width / 100);
			}
		}

		// if keep ratio is set to true, check output width/height and update them
		// as neccessary
		if ($this->keep_ratio && $output_width != null && $output_height != null)
		{
			$input_ratio = $input_width / $input_height;

			$output_ratio = $output_width / $output_height;

			if ($input_ratio > $output_ratio)
			{
				$output_height = $input_height * $output_width / $input_width;
			}
			else
			{
				$output_width = $input_width * $output_height / $input_height;
			}
		}

		// calculate missing width/height (if any)
		if ($output_width == null && $output_height == null)
		{
			$output_width = $input_width;
			$output_height = $input_height;
		}
		else
		if ($output_height == null)
		{
			$output_height = round(($output_width * $input_height) / $input_width);
		}
		else
		if ($output_width == null)
		{
			$output_width = round(($output_height * $input_width) / $input_height);
		}

		$src_x = 0;
		$src_y = 0;
		$src_w = $input_width;
		$src_h = $input_height;

		if ($this->enlarge == false && ($output_width > $input_width || $output_height > $input_height))
		{
			$output_width = $input_width;
			$output_height = $input_height;
		}
		else
		if ($crop == true)
		{
			if (($input_width / $input_height) > ($output_width / $output_height))
			{
				$ratio = $input_height / $output_height;
				$src_w = $ratio * $output_width;
				$src_x = round(($input_width - $src_w) / 2);
			}
			else
			{
				$ratio = $input_width / $output_width;
				$src_h = $ratio * $output_height;
				$src_y = round(($input_height - $src_h) / 2);
			}
		}

		$dst_im = imagecreatetruecolor($output_width, $output_height);

		if (!$dst_im)
		{
			imagedestroy($src_im);
			$this->error = 5;
			return false;
		}

		/* Check if this image is PNG or GIF and it's being saved as JPG, then set white background A.L. */
		if( (($input_extension == 'gif') || ($input_extension == 'png')) && $output_extension == 'jpg' ){
			$white = imagecolorallocate($dst_im, 255, 255, 255);
			imagefilledrectangle($dst_im, 0, 0, $output_width, $output_height, $white);
		}

		/* Check if this image is PNG or GIF, then set if Transparent R.L. */
		if( ($output_extension == 'gif') OR ($output_extension == 'png') )
		{
			imagealphablending($dst_im, false);
			imagesavealpha($dst_im,true);
			$transparent = imagecolorallocatealpha($dst_im, 255, 255, 255, 127);
			imagefilledrectangle($dst_im, 0, 0, $output_width, $output_height, $transparent);
		}

		$r = imagecopyresampled($dst_im, $src_im, 0, 0, $src_x, $src_y, $output_width, $output_height, $src_w, $src_h);

		if (!$r)
		{
			imagedestroy($src_im);
			$this->error = 6;
			return false;
		}

		switch ($output_extension)
		{
			case 'jpg':
			$r = imagejpeg($dst_im, $output_path, $this->quality);
			break;

			case 'png':
			$r = imagepng($dst_im, $output_path, $this->compression);
			break;

			case 'gif':
			$r = imagegif($dst_im, $output_path);
			break;
		}

		if (!$r)
		{
			imagedestroy($src_im);
			$this->error = 7;
			return false;
		}

		//chmod($output_path, $this->chmod);

		imagedestroy($src_im);

		return true;
	}

	/**
	 * Get file extension
	 *
	 * @param string $filename Filename
	 * @return string
	 */
	function getExtension($filename)
	{
		$pos = strrpos($filename, '.');

		if ($pos === false)
		{
			return '';
		}

		return strtolower(substr($filename, $pos + 1));
	}

	/**
	 * Get file extension by file type
	 *
	 * @param string $mimetype File type
	 * @return string
	 */
	function getFileExtensionByMimetype($mimetype)
	{
		switch ($mimetype)
		{
			case 'image/jpeg':
			case 'image/pjpeg':
			return 'jpg';

			case 'image/png':
			return 'png';

			case 'image/gif':
			return 'gif';

			default: return '';
		}
	}

}
?>