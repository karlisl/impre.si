<?php

namespace app\extensions\command;

use lithium\storage\Cache;

use app\models\Slides;

class Worker extends \lithium\console\Command {

	public $id = null;

    public function run() {
        if (!$this->id) {
			$this->id = rand(100, 999);
		}

		Cache::adapter('redis')->hset('worker.status', $this->id, 'started');
		Cache::adapter('redis')->hset('worker.status.last_time', $this->id, time());

		set_time_limit(0);

		$timeout = 60 * 60 * 1;
		$timeout += rand(0, 60 * 10);

		$start_time = time();

		$this->out(date('Y-m-d H:i:s') . '#INIT#' . $timeout);

		while (time() < $start_time + $timeout) {
			Cache::adapter('redis')->hset('worker.status', $this->id, 'empty');
			Cache::adapter('redis')->hset('worker.status.last_time', $this->id, time());

			$job = Cache::adapter('redis')->blpop(array(
					'queue.priority.high',
					'queue.priority.normal',
					'queue.priority.low',
				),
				10
			);

			if ($job && !empty($job[1])) {
				$this->out(date('Y-m-d H:i:s') . '#JOBSTART#' . $job[1]);
				Cache::adapter('redis')->hset('worker.status', $this->id, 'busy');
				Cache::adapter('redis')->hset('worker.status.last_time', $this->id, time());
				Cache::adapter('redis')->hset('worker.status.job_id', $this->id, $job[1]);

				$start = microtime(true);

				$result = $this->handle($job[1]);
				if ($result) {
					$this->out($result);
				}

				$end = microtime(true);
				$took = $end - $start;

				$this->out(date('Y-m-d H:i:s') . '#JOBEND#' . $job[1] . '#' . round($took, 2));
				Cache::adapter('redis')->hset('worker.status', $this->id, 'empty');
				Cache::adapter('redis')->hset('worker.status.last_time', $this->id, time());
			}
		}

		Cache::adapter('redis')->hset('worker.status', $this->id, 'closed');
		Cache::adapter('redis')->hset('worker.status.last_time', $this->id, time());

		$this->out(date('Y-m-d H:i:s') . '#TERM');
    }

	private function handle($id) {
		return Slides::generateFromCli($id);
	}
}
?>
