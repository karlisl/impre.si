<?php

namespace app\extensions\command;

use lithium\storage\Cache;

use app\models\Slides;

class Status extends \lithium\console\Command {

	public $id = null;

    public function run() {
		while (1) {
			$queue_lengths = array();
			$queue_lengths['high'] =Cache::adapter('redis')->command('llen', array('queue.priority.high'));
			$queue_lengths['normal'] = Cache::adapter('redis')->command('llen', array('queue.priority.normal'));
			$queue_lengths['low'] = Cache::adapter('redis')->command('llen', array('queue.priority.low'));

			$queue_total = 0;

			foreach($queue_lengths as $name => $size) {
				if ($size == null) {
					$queue_lengths[$name] = 0;
				}

				$queue_total += $queue_lengths[$name];
			}

			$workers = Cache::adapter('redis')->command('hgetall', array('worker.status'));
			$worker_times = Cache::adapter('redis')->command('hgetall', array('worker.status.last_time'));
			$worker_jobs = Cache::adapter('redis')->command('hgetall', array('worker.status.job_id'));
			ksort($workers);

			// Display Queue status
			echo "\n------------------------------------------------------------------\n";
			echo "Queue Statuses:\n\n";
			echo "	High:	".$queue_lengths['high']."\n";
			echo "	Normal:	".$queue_lengths['normal']."\n";
			echo "	Low:	".$queue_lengths['low']."\n\n";
			echo "	Total:	".$queue_total."\n\n";

			echo "Worker Statuses:\n\n";

			foreach ($workers as $worker_id => $status) {
				if ($worker_id > 0) {
					echo "	Worker [$worker_id]:	$status	".($status == 'busy' ? time() - $worker_times[$worker_id] : '')."	".($status == 'busy' ? $worker_jobs[$worker_id] : '')." \n";
				}
			}

			echo "------------------------------------------------------------------\n";

			// Sleep the delay
			sleep(1);
		}
    }
}
?>
