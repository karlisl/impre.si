<?php

namespace app\extensions;

use lithium\util\String;
use lithium\storage\Cache;

class Uploader {

	private static $_minFileSize = 1;
	private static $_maxFileSize = 20000000;
	private static $_acceptedExtensions = '/.pdf$/i';
	private static $_acceptedFileTypes = '/^application\/pdf$/i';
	
	public static function handle($upload) {
		$file = new File();

		$error = static::_hasErrors($upload);
		if ($error) {
			return $file->setError($error);
		}
		
		$id = String::uuid($_SERVER . time());
		
		clearstatcache();
		move_uploaded_file($upload['tmp_name'], LITHIUM_APP_PATH . DS . 'resources' . DS . 'tmp' . DS . 'upload' . DS . $id);
		
		$result = Cache::adapter('redis')->rpush('queue.priority.high', $id);

		if ($result) {
			$file->id = $id;
		} else {
			$file->setError('storageError');
		}

		return $file;
	}
	
	private static function _hasErrors($upload) {
		if (empty($upload)) {
			return 'emptyUpload';
		}
	
		if ($upload['error'] || !is_uploaded_file($upload['tmp_name'])) {
			return 'uploadError';
		}
		
		if (!preg_match(static::$_acceptedExtensions, $upload['name'])) {
            return 'acceptedExtensions';
        }

		if (!preg_match(static::$_acceptedFileTypes, mime_content_type($upload['tmp_name']))) {
            return 'acceptedFileTypes';
        }
		
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		if (!preg_match(static::$_acceptedFileTypes, finfo_file($finfo, $upload['tmp_name']))) {
            return 'acceptedFileTypes';
        }
		
		$filesize = filesize($upload['tmp_name']);
		if ($filesize < static::$_minFileSize) {
			return 'minFileSize';
		}

		if ($filesize > static::$_maxFileSize) {
			return 'maxFileSize';
		}
		
		return false;
	}
}

class File {

	public $id = null;
	public $error = null;
	
	public function isValid() {
		return empty($this->error);
	}
	
	public function setError($error) {
		$this->error = $error;
		return $this;
	}
}
?>