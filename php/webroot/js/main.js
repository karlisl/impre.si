window.app.collections.Slides = Backbone.Collection.extend({
	url: webroot + '/on/',

	parse: function() {

	}
});

/**
 * App Router
 */
window.app.Router = Backbone.Router.extend({

	routes: {
		'': 'index',
		'remote/:id/:slides/:count': 'remote',
		'on/*': 'on'
	},

	initialize: function() {
		this.AppView = new app.views.App();
		$('body').append(this.AppView.el);
	},

	index: function() {
		if ($(window).width() <= 979) {
			app.router.navigate('/on/', {trigger: true});
			return true;
		}

		if (!this.UploaderView) {
			this.UploaderView = new app.views.Uploader();
		}
		$('#content').html(this.UploaderView.el);

		this.UploaderView.showFrontpageTooltips();
		this.AppView.resize();
		this.UploaderView.initUploader();
	},

	on: function() {
		if (!this.OnView) {
			this.OnView = new app.views.On();
		}
		$('#content').html(this.OnView.el);
	},

	remote: function(id, slideId, count) {
		app.utils.slides.initRemote(id);

		var slides = $('<div id="slides" />');
		var slideClass = '';

		for (var s = 0; s < count; s++) {
			slideClass = 'slide';
			if (s == 0) slideClass += ' active';
			var img = $('<img />').attr('src', webroot + '/img/slides/' + slideId.substr(0, 3) + '/' + slideId + '/h/' + s + '.jpg');
			var el = $('<div />')
				.addClass(slideClass)
				.append(img);
			$(slides).append(el)
		}

		$(slides).appendTo('#content').each(function() {
			app.utils.slides.attachEvents();
			app.utils.slides.resizeSlides();
		});
	}
});

app.utils.templates.load(['app', 'uploader', 'on'], function() {
	app.router = new window.app.Router();
	Backbone.history.start({
		pushState: true
	});
});