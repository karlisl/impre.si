window.app = {
	models: {},
	collections: {},
	views: {},
	utils: {},
	router: {}
};

window.app.utils = {
	/**
	 * Template loader
	 */
	templates: {
		templates: {},

		load: function(names, callback) {

			var deferreds = [],
				self = this;

			$.each(names, function(index, name) {
				deferreds.push($.get(webroot + '/tpl/' + name, function (data) {
					self.templates[name] = data;
				}));
			});

			$.when.apply(null, deferreds).done(callback);
		},

		get: function(name) {
			return this.templates[name];
		}
	},

	/**
	 * Upload progress updater
	 */
	updateProgress: function(done, canvas_el, progress_el, flash) {
		var ctx = $(canvas_el)[0].getContext('2d');
		var w = 170;
		var h = 101;
		var r = 30;

		if (done > 1.0) {
			done = 1.0;
		}

		var alpha = (1-done)*(1-done);
		var start = -Math.PI/2 +
					 2*Math.PI/5 *
					 (0.2 + alpha) *
					 Math.sin(3*Math.PI * done);

		var a0 = start-Math.PI*done;
		var a1 = start+Math.PI*done;

		ctx.clearRect(0,0,w,h);

		ctx.lineCap = 'round';

		ctx.beginPath();
		ctx.strokeStyle = 'rgba(0,0,0,0.2)';
		ctx.lineWidth = 12;
		ctx.arc(w/2,h/2,r,0,Math.PI*2,true);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 4;
		ctx.strokeStyle = 'rgba(107,5,5,0.9)';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 5;
		ctx.strokeStyle = 'rgba(107,5,5,0.1)';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 8;
		ctx.strokeStyle = 'rgba(107,5,5,0.025)';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		$(progress_el).html(Math.round(100 * done) + '%');

		if (done == 1.0 && flash) {
			this.flashRing(0, canvas_el);
		}
	},

	/**
	 * Upload progress circle flasher
	 */
	flashRing: function (x, canvas_el) {
		var ctx = $(canvas_el)[0].getContext('2d');
		var w = 170;
		var h = 101;
		var r = 30;
		var a0 = 0;
		var a1 = 2*Math.PI;

		ctx.clearRect(0,0,w,h);

		ctx.lineCap = 'round';

		var f = 4*x*(1-x);
		var alpha = 0.9 + f * 0.1;

		ctx.beginPath();
		ctx.strokeStyle = 'rgba(0,0,0,0.2)';
		ctx.lineWidth = 12;
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 4;
		ctx.strokeStyle = 'rgba(107,5,5,0.9)';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 5;
		ctx.strokeStyle = 'rgba(107,5,5,0.1)';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 8;
		ctx.strokeStyle = 'rgba(107,5,5,0.025)';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 4;
		ctx.strokeStyle = 'rgba(255,255,255,' + f/2 +')';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 6;
		ctx.strokeStyle = 'rgba(107,5,5,' + f/4 + ')';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		ctx.beginPath();
		ctx.lineWidth = 15;
		ctx.strokeStyle = 'rgba(107,5,5,' + f/6 + ')';
		ctx.arc(w/2,h/2,r,a0,a1);
		ctx.stroke();

		var self = this;
		if (x < 1.0) {
			setTimeout(function () {
				self.flashRing(x + 0.05, canvas_el);
			}, 30);
		} else {
			var timeout = self.processing ? 80 : 1300;
			self.processing = true;

			setTimeout(function () {
				self.flashRing(0, canvas_el);
			}, timeout);
		}
	},

	request: {
		call: function(action, data, poll, callback) {
			$.ajax({
				cache: false,
				type: 'GET',
				url: 'http://94.100.6.22:8080/' + action,
				dataType: 'json',
				data: data,
				error: function (response) {
					if (poll) {
						setTimeout(function() {
							app.utils.request.call(action, data, poll, callback);
						}, 10 * 1000);
					}
				},
				success: function (response) {

					if (callback) {
						callback(response);
					}

					if (poll) {
						app.utils.request.call(action, data, poll, callback);
					} else {
						return response;
					}
				}
			});
		}
	},

	platform: {
		id: null,

		handleProcessing: function(result, callback) {
			app.utils.request.call('processing', result, false, function(data) {
				if (data.id) {
					app.utils.platform.id = data.id;
					app.utils.request.call('recv', {id: data.id}, true, function(data) {
						if (data.actions && data.actions.length) {
							callback();
							app.utils.platform.performActions(data.actions, callback);
						}
					})
				}
			});
		},

		stopPresentation: function() {
			app.utils.request.call('stop', {id: $('#presentation-id').val()}, false);
		},

		askQuestion: function() {
			app.utils.request.call('question', {id: $('#presentation-id').val()}, false);
		},

		performActions: function(actions, callback) {
			for (i = 0; i < actions.length; i++) {
				switch (actions[i].func) {
					case 'loadSlides':
						console.log('Load slide: ' + actions[i].data.id);
						$('#droplet').fadeOut(300);
						$('#droplet').remove();
						var slides = $('<div id="slides" />');
						var slideClass = '';

						for (var s = 0; s < actions[i].data.count; s++) {
							slideClass = 'slide';
							if (s == 0) slideClass += ' active';
							var img = $('<img />')
								.attr('src', webroot + '/img/slides/' + actions[i].data.id.substr(0, 3) + '/' + actions[i].data.id + '/h/' + s + '.jpg')
								.load(function() {
									app.utils.slides.resizeSlides();
								});
							var el = $('<div />')
								.addClass(slideClass)
								.append(img);
							$(slides).append(el)
						}

						var qr = $('<img />')
							.attr('src', webroot + '/qr/' + app.utils.platform.id + '/' + actions[i].data.id + '/' + actions[i].data.count)
							.load(function() {
								setTimeout(function() {
									$('#codes').addClass('focus');
									app.utils.slides.bindTooltips();
								}, 500);

								setTimeout(function() {
									if ($('#codes').hasClass('focus') && $('#codes').hasClass('initial')) {
										$('#codes').removeClass('focus');
										app.utils.slides.hideTooltips();
									}
								}, 5000);
							});

						var span = $('<span />').text(app.utils.platform.id.substr(0, 5));

						var a = $('<a />')
							.attr('href', webroot + '/remote/' + app.utils.platform.id + '/' + actions[i].data.id + '/' + actions[i].data.count)
							.attr('target', '_blank')
							.addClass('qr')
							.append(qr);

						var codes = $('<div />')
							.attr('id', 'codes')
							.addClass('initial')
							.on('mouseover', function() {
								$('#codes').removeClass('initial');
								if (!$('#codes').hasClass('focus')) {
									$(this).addClass('focus');
									app.utils.slides.showTooltips();
								}
							})
							.on('mouseleave', function() {
								if ($('#codes').hasClass('focus')) {
									$(this).removeClass('focus');
									app.utils.slides.hideTooltips();
								}
							})
							.append(a)
							.append(span);

						$('#content')
							.append(slides)
							.append(codes);

						$('#slides').append('<div id="messages"><div class="stop"></div><div class="question"></div></div>');

						app.utils.slides.attachEvents();

						break;

					case 'selectSlide':
						app.utils.slides.selectSlide(actions[i].data.slide);
						break;

					case 'stopPresentation':
						$('#messages .stop').fadeIn();
						break;

					case 'askQuestion':
						$('#messages .question').fadeIn();
						break;
				}
			}
		}
	},

	slides: {
		data: {},
		currentSlide: 0,
		isRemote: false,
		remoteId: null,

		nextSlide: function() {
			var slide = $('#slides').find('div.active');
			var nextSlide = $(slide).next('.slide');
			if (nextSlide.length) {
				$(slide).removeClass('active');
				$(nextSlide).addClass('active');
				app.utils.slides.currentSlide += 1;

				app.utils.slides.resizeSlides();

				if (app.utils.slides.isRemote) {
					app.utils.slides.remote();
				}
			}

			return false;
		},

		previousSlide: function() {
			var slide = $('#slides').find('div.active');
			var prevSlide = $(slide).prev('.slide');
			if (prevSlide.length) {
				$(slide).removeClass('active');
				$(prevSlide).addClass('active');
				app.utils.slides.currentSlide -= 1;

				app.utils.slides.resizeSlides();

				if (app.utils.slides.isRemote) {
					app.utils.slides.remote();
				}
			}

			return false;
		},

		selectSlide: function(slide) {
			if (!slide) {
				return false;
			}

			var el = $('#slides div.slide:eq(' + slide + ')');

			if (el.length) {
				el.siblings().removeClass('active');
				el.addClass('active');
				app.utils.slides.currentSlide = slide;

				app.utils.slides.resizeSlides();
			}
		},

		resizeSlides: function() {
			$('#messages div').hide();

			if ($(window).width() <= 979 || !$('#slides .active img').length) {
				return false;
			}

			if ($('#slides .active img').width()) {
				$('#slides .active').css('width', $('#slides .active img').width());
			} else {
				$('#slides .active img').on('load', function() {
					$('#slides .active').css('width', $('#slides .active img').width());
				});
			}
		},

		attachEvents: function() {
			$(window).resize(function() {
				app.utils.slides.resizeSlides();
			});

			$(window).ready(function() {
				app.utils.slides.resizeSlides();
			});

			$('body')
				/*.on('click', function() {
					if (fullScreenApi.isFullScreen()) {
						nextSlide();
						return false;
					}
				})*/
				.on('keydown', function(e) {
					var event = e || window.event;
					var keyCode = event.which || event.keyCode;

					if (keyCode == 37 || keyCode == 38) { // arrowLeft || arrowUp
						app.utils.slides.previousSlide();
						return false;
					} else if (keyCode == 39 || keyCode == 40 || keyCode == 32) { // arrowRight || arrowDown || space
						app.utils.slides.nextSlide();
						return false;
					}
				})
				.on('mousewheel', function(event, delta) {
					if (delta > 0) { // mouseWheelUp
						app.utils.slides.previousSlide();
					} else { // mouseWheelDown
						app.utils.slides.nextSlide();
					}
				});

			$('#slides')
				.touchwipe({
					wipeLeft: function() { app.utils.slides.nextSlide(); },
					wipeRight: function() { app.utils.slides.previousSlide(); },
					min_move_x: 20,
					min_move_y: 20
				})
				.on('dblclick', function () {
					if (fullScreenApi.supportsFullScreen) {
						if (fullScreenApi.isFullScreen()) {
							fullScreenApi.cancelFullScreen(document.getElementById('slides'));
							$('#slides').removeClass('fullscreen');
						} else {
							$('#slides').addClass('fullscreen');
							fullScreenApi.requestFullScreen(document.getElementById('slides'));
						}
					}
				});
		},

		bindTooltips: function() {
			$('#codes .qr').tooltip({
				title: 'This is presentation QR code.<br />Scan or click on it to control this presentation.',
				placement: 'left',
				trigger: 'manual'
			}).tooltip('show');
			$('#codes span').tooltip({
				title: 'This is audience code.<br />Open http://94.100.6.22/on and enter this code to take part in presentation process.',
				placement: 'bottom',
				trigger: 'manual'
			}).tooltip('show');
		},

		showTooltips: function() {
			$('#codes .qr').tooltip('show');
			$('#codes span').tooltip('show');
		},

		hideTooltips: function() {
			$('#codes .qr').tooltip('hide');
			$('#codes span').tooltip('hide');
		},

		initRemote: function(id) {
			app.utils.slides.isRemote = true;
			app.utils.slides.remoteId = id;
		},

		remote: function() {
			app.utils.request.call('remote', {
				id: app.utils.slides.remoteId,
				slide: app.utils.slides.currentSlide
			});
		}
	}
};
