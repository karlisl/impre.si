/**
 * App View
 */
window.app.views.App = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(app.utils.templates.get('app'));
		this.render();
		$(window).bind('resize.app', _.bind(this.resize, this));
	},

	render: function() {
		$(this.el).html(this.template());
        return this;
	},

	remove: function() {
		$(window).unbind('resize.app');
		Backbone.View.prototype.remove.call(this);
	},

	resize: function() {
		if ($(window).width() <= 979) {
			return false;
		}
		$(this.el).css('height', $(window).height());
		app.utils.slides.resizeSlides();
		//var img_height = $(window).height() < 512 ? 512 : $(window).height();
		//$('#slide img').css('height', img_height - 80);
	}
});

/**
 * Firstpage Uploader View
 */
window.app.views.Uploader = Backbone.View.extend({
	id: 'droplet',
	uploadDisabled: false,

	events: {
		'dragover': 'highlightDroplet',
		'dragleave': 'highlightDroplet',
		'drop': 'highlightDroplet'
	},

	initialize: function() {
		this.template = _.template(app.utils.templates.get('uploader'));
		this.render();
	},

	render: function() {
		$(this.el).html(this.template());
        return this;
	},

	initUploader: function() {
		var self = this;
		$('#droplet').fileupload({
			url: webroot + '/upload/',
			dropZone: $('#container'),
			maxFileSize: 20000000,
			maxChunkSize: 20000000,
			acceptFileTypes: /(.|\/)(pdf)$/i,
			singleFileUploads: true,
			add: function (e, data) {
				self.hideFrontpageTooltips();
				data.submit();
			},
			progress: function (e, data) {
				var progress = parseFloat(data.loaded / data.total, 10);
				app.utils.updateProgress(progress, $('#canvas'), $('#percent'));
			},
			sent: function (e, data) {
				if (self.uploadDisabled) {
					return false;
				}
				self.uploadDisabled = true;

				$('#picker').fadeOut(300, function () {
					$('#progress').fadeIn(300, function () {
						self.showUploadTooltip();
					});
				});

				app.utils.updateProgress(0, $('#canvas'), $('#percent'));
			},
			done: function (e, data) {
				if (!data.result.status || data.result.status != 'ok') {
					self.hideTooltips();
					self.shakePicker(1500);
				} else {
					self.showConvertTooltip();

					app.utils.updateProgress(1, $('#canvas'), $('#percent'), true);
					$('#percent').html('');
					app.utils.platform.handleProcessing(data.result, function(data) {
						self.hideTooltips();
					});
				}
			},
			fail: function (e, data) {
				console.log(data);
				console.log('error: ' + data.files[0].error);

				self.hideTooltips();

				if (self.uploadDisabled == false) {
					$('#picker').effect('shake', {}, 60);
				} else {
					self.shakePicker(1500);
				}
			}
		});
	},

	shakePicker: function(t) {
		var self = this;
		setTimeout(function () {
			$('#progress').fadeOut(300, function () {
				$('#picker').fadeIn(300, function () {
					$('#picker').effect('shake', {}, 60);
					self.uploadDisabled = false;
				});
			});
		}, t);
	},

	highlightDroplet: function(e) {
		if (this.uploadDisabled) {
			return false;
		}

		switch (e.type) {
			case 'dragover':
				$(this.el).addClass('focused');
				break;

			case 'dragleave':
			case 'drop':
				$(this.el).removeClass('focused');
				break;
		}
		e.preventDefault();
		return this;
	},

	showFrontpageTooltips: function() {
		$('.file-input').tooltip({
			title: 'Select presentation file.<br />For now we only support PDF\'s and the file size must no exceed 20MB.',
			trigger: 'hover'
		});
		/*setTimeout(function() {
			$('.file-input').tooltip('show');
		}, 1000);*/
	},

	showUploadTooltip: function() {
		$('#progress').tooltip({
			title: 'Uploading.',
			trigger: 'manual'
		});
		$('#progress').tooltip('show');
	},

	hideUploadTooltip: function() {
		$('#progress').tooltip('hide');
	},

	showConvertTooltip: function() {
		$('#progress').attr('data-original-title', 'Processing.<br />This may take a few minutes.').tooltip('fixTitle').tooltip('show');
	},

	hideConvertTooltip: function() {
		$('#progress').tooltip('hide');
	},

	hideFrontpageTooltips: function() {
		$('.file-input').tooltip('hide');
	},

	hideTooltips: function() {
		$('#progress').tooltip('hide');
		$('.file-input').tooltip('hide');
		$('.tooltip').remove();
	}
});

/**
 * Slide View
 */
window.app.views.Slides = Backbone.View.extend({
	id: 'slides',
	uploadDisabled: false,

	events: {},

	initialize: function() {
		this.template = _.template(app.utils.templates.get('uploader'));
		this.render();
	},

	render: function() {
		$(this.el).html(this.template());
        return this;
	},

	showTooltips: function() {}
});

/**
 * On View
 */
window.app.views.On = Backbone.View.extend({
	id: 'on',
	uploadDisabled: false,
	presentationId: null,

	events: {
		'click #stop-btn': 'stop',
		'click #question-btn': 'question'
	},

	initialize: function() {
		this.template = _.template(app.utils.templates.get('on'));
		this.render();
	},

	render: function() {
		$(this.el).html(this.template());
        return this;
	},

	stop: function() {
		app.utils.platform.stopPresentation();
		return false;
	},

	question: function() {
		app.utils.platform.askQuestion();
		return false;
	}
});
