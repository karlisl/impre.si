<?php
namespace app\models;

use lithium\security\Password;

/**
 * Users
 */
class Users extends \app\models\App {

	/**
	 * User login
	 *
	 * @param  array $data Post data (username and password)
	 * @return mixed User object or boolean false
	 */
	public static function login($data) {
		$user = static::find('first', array(
			'conditions' => array(
				'email' => $data['email']
			)
		));

		if (!empty($user) && Password::check($data['password'], $user->password)) {
			return $user;
		}

		return false;
	}
}
?>