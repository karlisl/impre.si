<?php

namespace app\models;

use lithium\util\String;
use lithium\storage\Cache;

use Imagick;
use ImagickException;

use RuntimeException;

/**
 * Slides
 */
class Slides extends \app\models\App {

	/**
	 * Sizes
	 *
	 * @var array
	 */
	public static $sizes = array(
		'h' => array(
			'width' => 1200,
			'height' => 1000,
			'compression' => 90
		),
		't' => array(
			'width' => 110,
			'height' => 80,
			'crop' => true,
			'compression' => 90
		)
	);

	/**
	 * Error messages
	 *
	 * @var array
	 */
	public static $errors = array(
		1 => 'Invalid file type'
	);

	/**
	 * Last error
	 *
	 * @var int
	 */
	static $lastErrorCode = null;

	/**
	 * Init GridFS
	 */
	static function __init() {
		parent::__init();
	}

	/**
	 * Return error message
	 *
	 * @param int $code Error code
	 * @return string Error message
	 */
	static function errors($code) {
		if (!empty(static::$errors[$code])) {
			return static::$errors[$code];
		}

		return $code . ': Unknown error';
	}

	/**
	 * Generate jpg from uploaded presentation file
	 *
	 * @param  string $id        Presentation file ID
	 * @param  array  $file_data Additional presentation data
	 * @return boolean Status
	 */
	public static function generate($id, array $file_data = array()) {
		$path = LITHIUM_APP_PATH . DS . 'resources' . DS . 'tmp' . DS . 'upload' . DS . $id;

		if (!file_exists($path)) {
			return false;
		}

		set_time_limit(180);

		try {
			$original = new Imagick();
			$original->setResolution(110, 110);
			$original->readImage($path);
		} catch (ImagickException $e) {
			self::$lastErrorCode = $e->getCode();
			return false;
		}

		$defaults = array(
			'crop' => false,
			'compression' => 60
		);

		$data = array(
			'data' => $file_data,
			'contentType' => 'image/jpeg',
			'uploadDate' => new MongoDate()
		);

		$count = $original->getNumberImages();

		if ($count) {
			for ($i = 0; $i < $count; $i++) {
				$data['filename'] = String::uuid($_SERVER) . '.jpg';

				foreach (static::$sizes as $key => $params) {
					$params = $params + $defaults;

					$copy = clone $original;
					$geometry = $copy->getImageGeometry();
					$copy->setiteratorindex($i);

					if ($params['crop'] && ($geometry['width'] > $params['width'] || $geometry['height'] > $params['height'])) {
						$copy->cropThumbnailImage($params['width'], $params['height']);
					} else {
						list($width, $height) = static::scaleImage(
							$geometry['width'],
							$geometry['height'],
							$params['width'],
							$params['height']
						);

						$copy->thumbnailImage($width, $height);
					}

					//$copy->sharpenImage(1,0.5);
					$copy->setImageFormat('jpg');
					$copy->setImageCompression(Imagick::COMPRESSION_JPEG);
					$copy->setImageCompressionQuality($params['compression']);

					$data['data'] = array_merge($data['data'], array(
						'size' => $key,
						'page' => $i
					));

					$id = static::store($copy, $data);

					if (!$id) {
						return false;
					}

					$copy->destroy();
				}
			}
		}

		return true;
	}

	/**
	 * Generate jpg from uploaded presentation file (called from CLI)
	 *
	 * @param  string $id        Presentation file ID
	 * @param  array  $file_data Additional presentation data
	 * @return boolean Status
	 */
	public static function generateFromCli($id, array $file_data = array()) {
		$path = LITHIUM_APP_PATH . DS . 'resources' . DS . 'tmp' . DS . 'upload' . DS . $id;

		if (!file_exists($path)) {
			return false;
		}

		set_time_limit(600);

		$defaults = array(
			'crop' => false,
			'compression' => 60
		);

		$parent_path = LITHIUM_APP_PATH . DS . 'webroot' . DS . 'img' . DS . 'slides' . DS . substr($id, 0, 3);
		$save_path = LITHIUM_APP_PATH . DS . 'webroot' . DS . 'img' . DS . 'slides' . DS . substr($id, 0, 3) . DS . $id;

		$count = exec('pdfinfo ' . $path . ' | grep Pages:');
		$count = (int) str_replace(array('Pages:', ' '), '', $count);

		if ($count) {
			if (!file_exists($parent_path)) {
				mkdir($parent_path, 0775);
			}
			if (!file_exists($save_path)) {
				mkdir($save_path, 0775);
			}

			$density = 100; // 120-150
			$command = 'convert -limit area 300mb -limit memory 300mb -density ' . $density . ' -alpha remove ' . $path;
			$total = count(static::$sizes);
			$i = 0;

			foreach (static::$sizes as $key => $params) {
				$params = $params + $defaults;
				$i++;

				if (!file_exists($save_path . DS . $key)) {
					mkdir($save_path . DS . $key, 0775);
				}

				if ($i != $total) {
					$command .= ' \( -clone 0--1 -scale ' . $params['width'] . 'x' . $params['height'] . ' -write ' . $save_path . DS . $key . DS . '%d.jpg -delete 0--1 \)';
				} else {
					$command .= ' -alpha remove -scale ' . $params['width'] . 'x -background white -gravity center -extent ' . $params['width'] . 'x' . $params['height'] . ' ' . $save_path . DS . $key . DS . '%d.jpg';
				}
			}

			$result = exec($command);
		}

		Cache::adapter('redis')->publish('completed-jobs', json_encode(array(
			'id' => $id,
			'count' => $count
		)));

		return $result;
	}

	/**
	 * Calculate valid edges for current size
	 *
	 * @param int $x Current width
	 * @param int $y Current height
	 * @param int $cx Prefered maximal width
	 * @param int $cy Prefered maxima; height
	 * @return array Sizes
	 */
	public static function scaleImage($x, $y, $cx, $cy) {
		list($nx, $ny) = array($x, $y);

		if ($x >= $cx || $y >= $cx) {
			if ($x > 0) {
				$rx = $cx / $x;
			}

			if ($y > 0) {
				$ry = $cy / $y;
			}

			if ($rx > $ry) {
				$r = $ry;
			} else {
				$r = $rx;
			}

			$nx = intval($x * $r);
			$ny = intval($y * $r);
		}

		return array($nx, $ny);
	}

	/**
	 * Check if valid JPG
	 *
	 * @param string $bytes
	 * @return boolean
	 */
	public static function checkJPG($bytes) {
		if (sprintf('%02X', ord(substr($bytes, -2, 1))) != 'FF' || sprintf('%02X', ord(substr($bytes, -1, 1))) != 'D9') {
			return false;
		}

		return true;
	}
}
?>
