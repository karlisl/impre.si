<?php
/**
 * Default layout
 */
?>
<!DOCTYPE html>
<html lang="<?=$locale?>" id="<?=$locale?>">
<head>
	<?php echo $this->html->charset() ?>
	<?php
		$title = $this->title();
	?>
	<title>Impresi<?php echo !empty($title) ? ' - ' . $title : '' ?></title>
	<?=$this->html->head('meta', array('options' => array('name' => 'description', 'content' => '')))?>
	<?=$this->html->head('meta', array('options' => array('name' => 'keywords', 'content' => '')))?>
	<?=$this->html->head('meta', array('options' => array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0')))?>

	<?php echo $this->html->style(array(
		'main.less'
	)) ?>

	<script>
		var webroot = '<?=$this->request()->get('env:base')?>',
			locale = '<?=$locale?>';
	</script>

	<?php echo $this->html->script(array(
		'json2',
		'jquery',
		'underscore',
		'backbone',
		'bootstrap-tooltip',
		'jquery.ui.widget',
		'jquery.iframe-transport',
		'jquery.fileupload',
		'jquery.fileupload-ui',
		'jquery.mousewheel',
		'jquery.swipe',
		'fullscreen',
		'utils',
		'views',
		'main'
	)) ?>
	<!--[if IE]>
 	<?php echo $this->html->script(array(
		'html5'
	)) ?>
	<![endif]-->

	<?php echo $this->html->link('Icon', null, array('type' => 'icon')); ?>

	<?=$this->_render('element', 'google_analytics')?>
</head>
<body>
</body>
</html>