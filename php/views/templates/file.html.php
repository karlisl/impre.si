<?php
/**
 * File uploader - file template
 */
?>
<div id="<%=id%>" class="file clearfix">
	<div class="info">
		<div class="progress-placeholder">
			<div class="progress"><div class="bar"><span>uploading 0%</span></div></div>
		</div>
		<div class="caption">Filename: <span><%=filename%></span></div>
		<div class="size">Size: <span><%=size%></span></div>
		<div class="controls"></div>
	</div>
</div>