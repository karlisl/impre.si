<?php
/**
 * Startpage uploader
 */
?>
<?=$this->form->create(null, array(
	'id' => 'picker',
	'url' => array(
		'controller' => 'pages',
		'action' => 'upload'
	),
	'type' => 'file'
))?>
	<span class="file-input">
		<span><?=$t('Select your presentation file')?></span>
		<?=$this->form->field('file', array(
			'type' => 'file',
			'label' => false,
			'template' => '{:label}{:input}'
		))?>
	</span>
	<span class="drop-text">or drag it here</span>
<?=$this->form->end()?>

<div id="progress">
	<canvas id="canvas" width="170" height="103"></canvas>
	<span id="percent">0%</span>
</div>