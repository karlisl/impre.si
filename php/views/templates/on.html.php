<?=$this->form->create()?>
	<?=$this->form->field('id', array(
		'type' => 'text',
		'id' => 'presentation-id',
		'label' => false,
		'placeholder' => $t('Presentation ID')
	))?>
<?=$this->form->end()?>
<?=$this->html->link($t('Pause'), 'Pages::on', array('id' => 'stop-btn', 'class' => 'on-btn'))?>
<?=$this->html->link($t('Ask question'), 'Pages::on', array('id' => 'question-btn', 'class' => 'on-btn'))?>
