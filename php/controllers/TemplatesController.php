<?php

namespace app\controllers;

/**
 * Templates controller
 */
class TemplatesController extends \app\controllers\AppController {

    public function _init() {
        parent::_init();
    }

    /**
     * Server template
     *
     * @param  string $template Template name
     * @return Render template
     */
	public function serve($template) {
		$this->_render['layout'] = 'blank';
		$this->_render['template'] = $template;
	}
}
?>