<?php
namespace app\controllers;

use lithium\storage\Session;
use lithium\action\DispatchException;

use app\models\Users;

/**
 * Users controller
 */
class UsersController extends \app\controllers\AppController {

	public function _init() {
		parent::_init();
	}

	public function login() {
		$user = Session::read('user', array('name' => 'default'));

		if (!empty($user)) {
			return $this->redirect(array(
				'controller' => 'admin',
				'action' => 'index'
			));
		}

		if (!empty($this->request->data)) {
			if ($user = Users::login($this->request->data)) {
				Session::write('user', $user, array('name' => 'default'));
				return $this->redirect(array(
					'controller' => 'pages',
					'action' => 'index',
					'admin' => true
				));
			} else {
				return $this->redirect('Users::login');
			}
		}

		$id = 'home';
		return compact('id');
	}

	public function admin_logout() {
		Session::delete('user', array('name' => 'default'));
		return $this->redirect(array(
			'controller' => 'pages',
			'action' => 'index',
			'admin' => null
		));
	}
}
?>