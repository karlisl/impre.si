<?php

namespace app\controllers;

use lithium\util\Set;
use lithium\storage\Session;
use lithium\core\Environment;

use app\models\App;
use app\models\Users;

/**
 * App controller
 */
class AppController extends \lithium\action\Controller {

	public $user = array();

	public function _init() {
		$this->_render['negotiate'] = true;
		parent::_init();
		$this->loadLocale();

		App::$user = $this->user = $user = Session::read('user', array('name' => 'default'));

		if (Session::check('query_log')) {
			Session::delete('query_log');
		}

		$this->set(array(
			'environment' => Environment::get(),
			'hostname' => Environment::get('hostname'),
			'locales' => Environment::get('locales'),
			'locale' => Environment::get('locale')
		));

		if ($this->request->admin) {
			$this->_render['layout'] = 'admin';
		}
	}

	/**
	 * Locales
	 */
	private function loadLocale() {
		App::$locale = $this->locale = Environment::get('locale');
		App::$locales = Environment::get('locales');
	}

	/**
	 * Redirect
	 *
	 * @param mixed $url Url
	 * @param array $options Options
	 */
	public function redirect($url, array $options = array('exit' => true)) {
		if ($this->request->is('ajax')) {
			$this->_render['data'] = array();
			return $this->render(array(
				'json' => array(
					'url' => \lithium\net\http\Router::match($url, $this->request)
				)
			));
		} else {
			return parent::redirect($url, $options);
		}
	}
}
?>