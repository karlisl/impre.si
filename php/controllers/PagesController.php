<?php

namespace app\controllers;

use lithium\storage\Session;

use app\models\Pages;
use app\models\Slides;

use app\extensions\Uploader;
use app\extensions\image\Resizer;

use \QRcode;

use Imagick;

/**
 * Pages controller
 */
class PagesController extends \app\controllers\AppController {

	public function _init() {
		parent::_init();
	}

	public function index() {}

	/**
	 * Presentation upload
	 *
	 * @return json response
	 */
	public function upload() {
		$file = Uploader::handle($this->request->data['file']);

		if ($file->isValid()) {
			$return = array(
				'status' => 'ok',
				'id' => $file->id
			);

			Session::write('last_upload_status', 'ok');
			Session::write('last_upload_id', $file->id);
		} else {
			$return = array(
				'status' => 'error',
				'error' => $file->error
			);

			Session::write('last_upload_status', 'error');
		}

		header('Cache-Control: no-cache, must-revalidate' );
		header('Pragma: no-cache');
		header('Content-type: text/plain');
		echo json_encode($return);
		exit;

		$this->_render['data'] = array();
		return $this->render(array(
			'json' => $return
		));
	}

	public function on($id) {
		return $this->index();
	}

	public function remote($id) {
		return $this->index();
	}

	public function qr($id, $slide_id, $count) {
		QRcode::png('http://94.100.6.22/remote/' . $id . '/' . $slide_id . '/' . $count);
		exit;
	}

	private function checkJPG($bytes) {
		if (sprintf('%02X', ord(substr($bytes, -2, 1))) != 'FF' || sprintf('%02X', ord(substr($bytes, -1, 1))) != 'D9') {
			return false;
		}

		return true;
	}
}
?>
