(function() {
	HOST = null; // localhost
	PORT = 8080;

	var starttime = (new Date()).getTime();

	var mem = process.memoryUsage();
	setInterval(function () {
		mem = process.memoryUsage();
	}, 10 * 1000);

	var fu = require('./fu'),
		sys = require('util'),
		url = require('url'),
		qs = require('querystring'),
		crypto = require('crypto'),
		io = require('socket.io')
		redis = require('redis'),
		redisClient = redis.createClient(),
		redisSubscribeClient = redis.createClient();
		/*express = require('express');*/

	var SAVED_ACTIONS = 50;
	var CALLBACK_RESET_TIME = 30; // seconds

	var sessions = {};
	var queues = {};
	var completedJobs = {};

	redisSubscribeClient.subscribe('completed-jobs');
	redisSubscribeClient.on('message', function (channel, data) {
		data = JSON.parse(data);
		var id = data.id;

		if (typeof queues[id] != 'undefined') {
			if (sessions[queues[id]].status == 'processing') {
				sessions[queues[id]].performAction('loadSlides', {
					'id': id,
					'count': data.count
				});
				sessions[queues[id]].status = 'presenting';
			}

			delete queues[id];
		} else {
			completedJobs[id] = id;
		}
    });

	var Session = function (data) {
		this.id = data.id;
		this.queue_id = data.queue_id ? data.queue_id : 0
		this.currentSlide = data.currentSlide ? data.currentSlide : 0;
		this.status = 'processing';

		this.actions = [];
		this.callbacks = [];

		if (!sessions.hasOwnProperty(data.id)) {
			this.create();
		}

		var $this = this;

		// clear old callbacks and actions
		setInterval(function () {
			var now = new Date();
			while ($this.callbacks.length > 0 && now - $this.callbacks[0].timestamp > CALLBACK_RESET_TIME * 1000) {
				$this.callbacks.shift().callback([]);
			}
			while ($this.actions.length > 0 && now - $this.actions[0].sent > CALLBACK_RESET_TIME * 1000) {
				$this.actions.shift();
			}
		}, 3000);
	}

	Session.prototype = {
		create: function () {
			sessions[this.id] = this;
			return this;
		},

		query: function (callback) {
			var matching = [];

			for (var i = 0; i < this.actions.length; i++) {
				if (!this.actions[i].sent) {
					matching.push(this.actions[i]);
					this.actions[i].sent = new Date();
				}
			}

			if (matching.length != 0) {
				callback(matching);
			} else {
				this.callbacks.push({
					timestamp: new Date(),
					callback: callback
				});
			}

			this.save();
		},

		performAction: function (func, data) {
			var action = {
				'func': func,
				'data': data,
				'sent': false
			};

			this.actions.push(action);

			while (this.callbacks.length > 0 && !action.sent) {
				action.sent = new Date();
				this.callbacks.shift().callback([action]);
			}

			while (this.actions.length > SAVED_ACTIONS) {
				this.actions.shift();
			}
		},

		save: function () {
			sessions[this.id] = this;
			return this;
		},

		destroy: function () {
			delete sessions[this.id];
			return true;
		}
	};

	/**
	 * Listener
	 */
	fu.listen(Number(process.env.PORT || PORT), HOST);

	fu.get('/recv', function (req, res) {
		var data = qs.parse(url.parse(req.url).query);
		var session = sessions[data.id];

		if (typeof session == 'undefined') {
			return res.simpleJSON(400, {
				error: 'invalid session'
			});
		}

		redisClient.hset('on.keys', data.id.substr(0, 5), data.id);

		if (session.status == 'processing' && completedJobs[session.queue_id]) {
			session.performAction('loadSlides', {
				'id': completedJobs[session.queue_id]
			});
			sessions[data.id].status = 'presenting';
			delete completedJobs[session.queue_id];
		}

		session.query(function (actions) {
			return res.simpleJSON(200, {
				actions: actions
			});
		});
	});

	fu.get('/remote', function (req, res) {
		var id = qs.parse(url.parse(req.url).query).id;
		var slide = qs.parse(url.parse(req.url).query).slide;

		if (sessions.hasOwnProperty(id)) {
			sessions[id].performAction('selectSlide', {
				'slide': slide
			});
		}

		res.simpleJSON(200, {});
	});

	fu.get('/processing', function (req, res) {
		var queue_id = qs.parse(url.parse(req.url).query).id;
		var data = {
			id: crypto.createHash('sha1').update(Math.floor(Math.random()*99999999999).toString() + new Date() + queue_id).digest('hex'),
			queue_id: queue_id
		};

		var session = new Session(data);
		queues[queue_id] = session.id;

		console.log('new session: ' + session.id);

		res.simpleJSON(200, {
			id: session.id
		});
	});

	fu.get('/stop', function (req, res) {
		var id = qs.parse(url.parse(req.url).query).id.toLowerCase();
		console.log(id);

		redisClient.hget('on.keys', id, function(err, res) {
			if (!err && res && sessions.hasOwnProperty(res)) {
				sessions[res].performAction('stopPresentation', {});
			}
		});

		res.simpleJSON(200, {});
	});

	fu.get('/question', function (req, res) {
		var id = qs.parse(url.parse(req.url).query).id.toLowerCase();
		console.log(id);

		redisClient.hget('on.keys', id, function(err, res) {
			if (!err && res && sessions.hasOwnProperty(res)) {
				sessions[res].performAction('askQuestion', {});
			}
		});

		res.simpleJSON(200, {});
	});

}).call(this);
